FROM docker.io/rust:1.65.0-alpine3.16 as build
ARG PROJECT_NAME

RUN apk add --update-cache --no-cache musl-dev perl make

# create a new empty shell project
RUN cargo new --bin ${PROJECT_NAME}
WORKDIR /${PROJECT_NAME}

# copy over your manifests
COPY ./.cargo/config.toml ./.cargo/config.toml
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# this build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# copy your source tree
COPY ./src ./src

# build for release
RUN rm ./target/release/deps/${PROJECT_NAME}*
RUN cargo build --release

# our final base
FROM registry.gitlab.com/xangelix-pub/containers:arch-rust-latest
ARG PROJECT_NAME

ENV ENV_PROJECT_NAME=$PROJECT_NAME
WORKDIR /${PROJECT_NAME}

RUN paru -Syu --noconfirm spass

# copy the build artifact from the build stage
COPY --from=build /${PROJECT_NAME}/target/release/${PROJECT_NAME} /${PROJECT_NAME}/${PROJECT_NAME}

# set the startup command to run your binary
CMD "./$ENV_PROJECT_NAME"
