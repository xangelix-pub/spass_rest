#[macro_use]
extern crate log;
extern crate base64;

use dotenv::dotenv;
use actix_cors::Cors;
use actix_web::{post, http, web, App, HttpRequest, HttpResponse, HttpServer, Result};
use listenfd::ListenFd;
use serde::{Deserialize};
use std::env;
use std::io::{Write};
use std::process::{Command, Stdio};

#[derive(Deserialize)]
struct SPASSRequest {
    api_key: String,
    spass: String,
}

fn z(spass: String) -> String {
    let mut child = Command::new("SPASS")
        .args(["-Stdin"])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn().expect("Could execute SPASS.");

    let child_stdin = child.stdin.as_mut().unwrap();
    // let spass_problem_fd = fs::read("problem.txt").expect("Unable to read input file");
    // let spass_problem_str = String::from_utf8(spass_problem_fd).unwrap();
    //child_stdin.write_all(spass_problem_str.as_bytes())?;

    child_stdin.write_all(spass.as_bytes()).expect("Could not write to SPASS.");

    // Close stdin to finish and avoid indefinite blocking
    drop(child_stdin);

    let spass_problem_std = child.wait_with_output().expect("Could read from SPASS.");
    let spass_problem_stdout = String::from_utf8(spass_problem_std.stdout).unwrap();

    // let mut result = "none";
    // for line in spass_problem_stdout.lines() {
    //     if line.contains("SPASS beiseite: Proof found.") {
    //         result = "proof";
    //     } else if line.contains("SPASS beiseite: Completion found.") {
    //         result = "completion";
    //     }
    // }
    //println!("{}", result);
    //Ok(())
    return spass_problem_stdout.to_string();
}

fn get_api_key() -> String {
    match env::var("API_KEY") {
        Ok(lang) => return lang,
        Err(e) => {
            println!("Couldn't read API_KEY ({})", e);
            std::process::exit(exitcode::CONFIG);
        }
    };
}

#[post("/request")]
async fn ep_request(req: web::Json<SPASSRequest>) -> Result<String> {
    println!("{} {}", req.api_key, req.spass);
    let api_key = get_api_key();

    if req.api_key == api_key {
        let bytes = base64::decode(req.spass.clone()).unwrap();
        let spass = String::from_utf8(bytes).unwrap();
        let response = z(spass);
        
        Ok(format!("Welcome {}!\n{}\n", api_key, response))
    } else {
        Ok(format!("Invalid api_key."))
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    env_logger::init();

    let host = env::var("HOST").expect("Host not set");
    let port = env::var("PORT").expect("Port not set");

    let mut server = HttpServer::new(|| {
        let cors = Cors::default().allow_any_origin().send_wildcard();

        App::new()
            .wrap(cors)
            .service(ep_request)
    })
    .bind(format!("{}:{}", host, port))?
    .run()
    .await;

    info!("Starting server");
    Ok(())
}

