# SPASS REST

## Deployment

1. Change settings including API_KEY in `.env`
2. `podman-compose up`
3. ???
4. Profit

## Use

POST requests should look like this to the endpoint `/request`:

```json
{
    "api_key":"<API_KEY>",
    "spass": "<SPASS | base64 -w0>"
}
```
