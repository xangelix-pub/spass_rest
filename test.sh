#!/usr/bin/env bash

curl -X POST 127.0.0.1:5000/request -H 'Content-Type: application/json' -d '{"api_key":"'$(cat .env | grep API_KEY | cut -d '=' -f 2 )'","spass": "'$(cat $1 | base64 -w0)'"}'
